const initialState = {
  data: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD":
      return {
        ...state,
        data: [
          ...state.data.slice(0, action.index),
          Number(state.data[action.index]) + 1,
          ...state.data.slice(action.index + 1)
        ]
      };
    case "SUB":
      return {
        ...state,
        data: [
          ...state.data.slice(0, action.index),
          Number(state.data[action.index]) - 1,
          ...state.data.slice(action.index + 1)
        ]
      };
    case "DELETE":
      return {
        ...state,
        data: [
          ...state.data.slice(0, action.index),
          ...state.data.slice(action.index + 1)
        ]
      };
    case "ADD-COUNTER":
      return { ...state, data: [...state.data, 0] };
    default:
      return state;
  }
};

export default reducer;
