import React, { Component } from "react";
import "./App.css";

import { connect } from "react-redux";

class App extends Component {
  render() {
    let toPrint = this.props.data.map((number, index) => (
      <div key={index}>
        <p className="number">{number}</p>
        <div className="buttons">
          <button
            className="button pos-button"
            onClick={() => this.props.increase(index)}
          >
            +
          </button>
          <button
            className="button neg-button"
            onClick={() => this.props.decrease(index)}
          >
            -
          </button>
          <button
            className="button cross-button"
            onClick={() => this.props.handleDelete(index)}
          >
            X
          </button>
        </div>
      </div>
    ));
    return (
      <div>
        {toPrint}
        <button className="add-counter" onClick={this.props.addCounter}>
          Add Counter
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};

const mapDispatchToProps = dispatch => {
  return {
    increase: index =>
      dispatch({
        type: "ADD",
        index
      }),
    decrease: index =>
      dispatch({
        type: "SUB",
        index
      }),
    handleDelete: index =>
      dispatch({
        type: "DELETE",
        index
      }),
    addCounter: () =>
      dispatch({
        type: "ADD-COUNTER"
      })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
